CKEDITOR.plugins.add( 'geshi',
{
	requires : [ 'styles', 'button' ],

	init : function( editor )
	{
		// All buttons use the same code to register. So, to avoid
		// duplications, let's use this tool function.
		var addButtonCommand = function( buttonName, buttonLabel, commandName, styleDefiniton )
		{
			var style = new CKEDITOR.style( styleDefiniton );

			editor.attachStyleStateChange( style, function( state )
				{
					editor.getCommand( commandName ).setState( state );
				});

			editor.addCommand( commandName, new CKEDITOR.styleCommand( style ) );

			editor.ui.addButton( buttonName,
				{
					label : buttonLabel,
					command : commandName
				});
		};

		var config = editor.config;
//		var lang = editor.lang;

		addButtonCommand( 'Geshi-code'		, 'code'		, 'Geshi-code'	, config.coreStyles_code );
		addButtonCommand( 'Geshi-php'		, 'php'			, 'Geshi-php'		, config.coreStyles_php );
		addButtonCommand( 'Geshi-bash'		, 'bash'		, 'Geshi-bash'	, config.coreStyles_bash );
		addButtonCommand( 'Geshi-html'		, 'html'		, 'Geshi-html'		, config.coreStyles_html );
		addButtonCommand( 'Geshi-css'		, 'css'			, 'Geshi-css'	, config.coreStyles_css );
	}
});

// Basic Inline Styles.
CKEDITOR.config.coreStyles_code			= { element : 'pre' };
CKEDITOR.config.coreStyles_php			= { element : 'pre', attributes : { 'language' : 'php' } };
CKEDITOR.config.coreStyles_bash			= { element : 'pre', attributes : { 'language' : 'bash' } };
CKEDITOR.config.coreStyles_html			= { element : 'pre', attributes : { 'language' : 'html4strict' } };
CKEDITOR.config.coreStyles_css			= { element : 'pre', attributes : { 'language' : 'css' } };
